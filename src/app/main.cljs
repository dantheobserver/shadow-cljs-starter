(ns app.main
  (:require [goog.dom :as gdom]
            [reagent.dom :as dom]))

(defonce on-devcards-page?
  (= (.. js/window -location -pathname)
     "/devcards.html"))

(defn mount []
  (when-not on-devcards-page?
    (dom/render
     [:div "App component here"]
     (gdom/getElement "app"))))

(defn ^:dev/after-load start []
  (mount))

(defn main []
  (mount))

