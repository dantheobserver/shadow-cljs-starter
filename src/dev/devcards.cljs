(ns dev.devcards
  (:require [reagent.core :as r]
            [devcards.core
             :as devcards
             :include-macros true
             :refer [defcard defcard-rg]]))

(defonce on-devcards-page?
  (= (.. js/window -location -pathname)
     "/devcards.html"))

(defonce _
  (when on-devcards-page?
    (devcards/start-devcard-ui!)))

(defn sample-component []
  [:div "Your Component here"])

(defcard-rg sample-component
  sample-component)
