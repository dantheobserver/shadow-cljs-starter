(ns dev.hooks
  (:require [clojure.java.shell :as shell]
            [clojure.string :as str]))

;; The possible stages the :build-hooks can use are:
;; :configure - initial :target specific configuration
;; :compile-prepare - called before any compilation is done
;; :compile-finish - called after all compilation finishes
;; :optimize-prepare - called before running the Closure Compiler optimization phase (:release only)
;; :optimize-finish - called after Closure is done (:release only)
;; :flush - called after everything was flushed to disk
(def running-cmds (atom {}))

(defn run-npx-cmd
  {:shadow.build/stage :configure}
  [build-state & [cmd]]
  (let [npx-cmd (str "npx " cmd)
        arg-list (str/split npx-cmd #"\s+")]
    (prn "Running command: " npx-cmd)
    (apply shell/sh arg-list)
    build-state))
